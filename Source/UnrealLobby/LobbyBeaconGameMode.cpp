// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyBeaconGameMode.h"
#include "UnrealLobby.h"
#include "OnlineSessionSettings.h"
#include "GameFramework/GameSession.h"
#include "OnlineSessionInterface.h"
#include "Online.h"
#include "OnlineSubsystemNull.h"
#include "UnrealLobby.h"
#include "MyGameInstance.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/PlayerController.h"
#include "Engine/LocalPlayer.h"

#include "UnrealLobbyGameMode.h"

#include "LobbyBeaconState.h"
#include "LobbyBeaconClient.h"

#include "Online.h"
#include "CoreOnline.h"






void ALobbyBeaconGameMode::BeginPlay()
{
	Super::BeginPlay();


	if (!BeaconHost)
	{
		print("DYNAMIC CREATE BEACON HOST");

		CreateBeaconHost();
	}


}



void ALobbyBeaconGameMode::CreateBeaconHost()
{
	print("CRETING BEACON HOST");

	BeaconHost = GetWorld()->SpawnActor<AMyOnlineBeaconHost>(AMyOnlineBeaconHost::StaticClass());

	BeaconHost->Start();
}




void ALobbyBeaconGameMode::HostLobby()
{

	print("HOST LOBBY SERVER!!!!");

	TSharedPtr<const FUniqueNetId> UserId;

	auto OnlineSubsystem = IOnlineSubsystem::Get();
	if (OnlineSubsystem)
	{
		auto IdentityInterface = OnlineSubsystem->GetIdentityInterface();
		if (IdentityInterface.IsValid())
		{
			UserId = IdentityInterface->GetUniquePlayerId(0);

			print_param("UserId in ServerHostLobby_Implementation vrplayer!!11 %s", *(*UserId).ToString());

			TSharedPtr<const FUniqueNetId> UserIdTest = IdentityInterface->GetUniquePlayerId(1);

			if (UserIdTest.IsValid())
			{
				print_param("UserId in ServerHostLobby_Implementation  vrplayer!!22 %s", *(*UserIdTest).ToString());

			}

			UserIdTest = IdentityInterface->GetUniquePlayerId(2);


			if (UserIdTest.IsValid())
			{
				print_param("UserId in ServerHostLobby_Implementation  vrplayer!!33 %s", *(*UserIdTest).ToString());

			}

		}
	}

	//UMyGameInstance* GI = Cast<UMyGameInstance>(GetGameInstance());

//	if (GI)
//	{
		print("in  HOST ");

		FString const StartURLdd = FString::Printf(TEXT("/Game/Maps/MultiplayerTest/%s?game=%s%s%s?%s=%d%s"), "LobbyMap" /**GetMapName()*/, TEXT(""), TEXT("?listen") /*TEXT("")*/, /*TEXT("?bIsLanMatch")*/ TEXT(""), TEXT(""), 0, TEXT(""));
		print_param("FF StartURL: %s", *StartURLdd);


		FString const StartURL = FString::Printf(TEXT("/Game/Level/%s?game=%s%s%s?%s=%d%s"), "MapA" /**GetMapName()*/, TEXT(""), TEXT("?listen") /*TEXT("")*/, /*TEXT("?bIsLanMatch")*/ TEXT(""), TEXT(""), 0, TEXT(""));


		print_param("HOSTMAPNAME StartURL: %s", *StartURL);

		//	*GetGameInstance()->GetLocalPlayerByIndex(0)->GetPreferredUniqueNetId()

		// Game instance will handle success, failure and dialogs
		FName GameName = GameSessionName;

		FName FinalSessionName = FName(*(GameName.ToString() + FString::FromInt(PartySessionsConter)));


		//	*GetGameInstance()->GetLocalPlayerByIndex(0)->GetPreferredUniqueNetId()

		// Game instance will handle success, failure and dialogs
		HostGame(FinalSessionName, StartURL);
		print("out ");

		//}
		print("out ");

//	}


	//////////////////////////////////////////////////////////////

	print("server HOSTING LOBBY  ");

	//	CreateBeaconHost();

	//HOST LOBBY
	//////////////////////////////////////////////////////////////////////////////////////	
	print("CRETING LOBBY HOST");


	//if (!GEngine->GetWorld())
	//{
	//	print("NO enginenoeoenoenoeeoefgz");
	//	}
	UWorld* World = GetWorld();

	//	print("CREEPY WORLD");

	if (!World)
	{
		print("NO WORLDDDDD");
		return;
	}


	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.Instigator = Instigator;

	LobbyHosts.Add(GetWorld()->SpawnActor<AMyLobbyBeaconHostObject>(AMyLobbyBeaconHostObject::StaticClass()));
	print("AFTER CCC Lobby HOST");



	if (LobbyHosts[LobbySessionsConter])
	{
		print("add BEACON LobbyHost");
		//	bool returnHost = LobbyHost->InitHostBeacon(2, 3, 100, NAME_GameSession, 0);

		bool returnHost = LobbyHosts[LobbySessionsConter]->Init(FinalSessionName);

		LobbyHosts[LobbySessionsConter]->SetupLobbyState(100);

		print("INIT BEACON PartyHost2");
		EBeaconState::Type state = LobbyHosts[LobbySessionsConter]->GetBeaconState();

		if (returnHost)
		{
			//	print_param("RETURN HOST SYUCCESSFUL %i", state);
			print_param("LobbyHost->GetLevel()->GetName(); %s", *LobbyHosts[LobbySessionsConter]->GetLevel()->GetName());
			//	print_param("		LobbyHost->GetState()->GetNumConsumedReservations()); %i", LobbyHost->GetState()->GetNumConsumedReservations());
			//	print_param("		LobbyHost->GetState()->GetReservationCount()); %i", LobbyHost->GetState()->GetReservationCount());


			//	GetWorld()->GetAuthGameMode<AStarterGameMode>()->AddHostToBeaconHost(PartyHost);

			print_param("LobbySessionsConter %i", LobbySessionsConter);


			AddHostToBeaconHost(LobbyHosts[LobbySessionsConter]);


			//		BeaconHost->AddHost(PartyHost);
			LobbySessionsConter++;
		}
		else
		{
			print("RETURN HOST FAILEDDDDD");
			LobbyHosts.RemoveAt(LobbySessionsConter);

		}
	

	}
	else
	{
		print("THERE IS NO LobbyHost  SPAWNED!!!!");


	}
}


void ALobbyBeaconGameMode::AddHostToBeaconHost(AOnlineBeaconHostObject* Host)
{

	print("ADDING A HOST TO BEACON HOST");

	BeaconHost->AddHost(Host);
}





void ALobbyBeaconGameMode::StartPartyHost()
{
	/*

	TSharedPtr<const FUniqueNetId> UserId;

	auto OnlineSubsystem = IOnlineSubsystem::Get();
	if (OnlineSubsystem)
	{
		auto IdentityInterface = OnlineSubsystem->GetIdentityInterface();
		if (IdentityInterface.IsValid())
		{
			UserId = IdentityInterface->GetUniquePlayerId(0);

			UE_LOG(LogOnlineGame, Warning, TEXT("UserId in ServerHostParty_Implementation vrplayer!!11 %s"), *(*UserId).ToString());

			TSharedPtr<const FUniqueNetId> UserIdTest = IdentityInterface->GetUniquePlayerId(1);

			if (UserIdTest.IsValid())
			{
				UE_LOG(LogOnlineGame, Warning, TEXT("UserId in ServerHostParty_Implementation  vrplayer!!22 %s"), *(*UserIdTest).ToString());
			}

			UserIdTest = IdentityInterface->GetUniquePlayerId(2);


			if (UserIdTest.IsValid())
			{
				UE_LOG(LogOnlineGame, Warning, TEXT("UserId in ServerHostParty_Implementation  vrplayer!!33 %s"), *(*UserIdTest).ToString());
			}

		}
	}
	*/
//UMyGameInstance* GI = Cast<UMyGameInstance>(GetGameInstance());
	FString const StartURL = FString::Printf(TEXT("/Game/Level/%s?game=%s%s%s?%s=%d%s"), "MapA" /**GetMapName()*/, TEXT(""), TEXT("?listen") /*TEXT("")*/, /*TEXT("?bIsLanMatch")*/ TEXT(""), TEXT(""), 0, TEXT(""));

//	if (GI)
	//{
		print("in  HOST ");

		FName PartyName = PartySessionName;

		FName FinalSessionName = FName(*(PartyName.ToString() + FString::FromInt(PartySessionsConter)));


		//	*GetGameInstance()->GetLocalPlayerByIndex(0)->GetPreferredUniqueNetId()

		// Game instance will handle success, failure and dialogs
		HostGame(FinalSessionName, StartURL);
		print("out ");

	//}


	//////////////////////////////////////////////////////////////

	print("server HOSTING PARTYY  ");

	//	CreateBeaconHost();

	//HOST PARTY
	//////////////////////////////////////////////////////////////////////////////////////	
	print("CRETING PARTY HOST");



	UWorld* World = GetWorld();

	if (!World)
	{
		print("NO WORLDDDDD");
		return;
	}

//	FActorSpawnParameters SpawnParams;
//	SpawnParams.Owner = this;
//	SpawnParams.Instigator = Instigator;

	PartyHosts.Add(GetWorld()->SpawnActor<AMyPartyBeaconHost>(AMyPartyBeaconHost::StaticClass()));
	print("AFTER CCC PARTY HOST");

	if (PartyHosts[PartySessionsConter])
	{
		print("add BEACON PartyHost");
		bool returnHost = PartyHosts[PartySessionsConter]->InitHostBeacon(2, 3, 100, FinalSessionName, 0);



		print("INIT BEACON PartyHost2");
		EBeaconState::Type state = PartyHosts[PartySessionsConter]->GetBeaconState();

		if (returnHost)
		{
			//			print_param("RETURN HOST SYUCCESSFUL %i", state);
			print_param("PartyHost->GetLevel()->GetName(); %s", *PartyHosts[PartySessionsConter]->GetLevel()->GetName());
			print_param("		PartyHost->GetState()->GetNumConsumedReservations()); %i", PartyHosts[PartySessionsConter]->GetState()->GetNumConsumedReservations());
			print_param("		PartyHost->GetState()->GetReservationCount()); %i", PartyHosts[PartySessionsConter]->GetState()->GetReservationCount());


			//	GetWorld()->GetAuthGameMode<AStarterGameMode>()->AddHostToBeaconHost(PartyHost);

			AddHostToBeaconHost(PartyHosts[PartySessionsConter]);
			print_param("PartySessionsConter %i", PartySessionsConter);

			PartySessionsConter++;
			//		BeaconHost->AddHost(PartyHost);
		}
		else
		{
			print("RETURN HOST FAILEDDDDD");
			PartyHosts.RemoveAt(PartySessionsConter);


		}




	}
	else
	{
		print("THERE IS NO PARTy  SPAWNED!!!!");


	}


}




// starts playing a game as the host
bool ALobbyBeaconGameMode::HostGame(FName InSessionName, const FString& InTravelURL)
{
	
	print("HostGameHostGameHostGame");
	print("SET");

	print_param("SET2 %s ", *InTravelURL);

	TravelURL = InTravelURL;
	print("SET");

	bool const bIsLanMatch = InTravelURL.Contains(TEXT("?bIsLanMatch"));
	print("AFTERSET");

	//determine the map name from the travelURL
	const FString& MapNameSubStr = "/Game/Level/";
	const FString& ChoppedMapName = InTravelURL.RightChop(MapNameSubStr.Len());
	const FString& MapName = ChoppedMapName.LeftChop(ChoppedMapName.Len() - ChoppedMapName.Find("?game"));
	print("lll");

	
	print_param("HOSTMAPNAME: %s", *MapName);


	if (HostSession(InSessionName, "", MapName, bIsLanMatch, true, 100))
	{
		print("SESSION HOSTEDDD");
	
		return true;
	}
	else print("ERROR HOSTING SEESISISIOSOSOS HOSTEDDD");
	//}

	return false;
}



bool ALobbyBeaconGameMode::HostSession(FName InSessionName, const FString& GameType, const FString& MapName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers)
{
	print("HostSession");




	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{

		//	MaxPlayers = MaxNumPlayers;

		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			print("ALL VALID!!!!!!!");

			HostSettings = MakeShareable(new FOnlineSessionSettings());

			HostSettings->bIsLANMatch = bIsLAN;
			HostSettings->bUsesPresence = bIsPresence;
			HostSettings->NumPublicConnections = MaxNumPlayers;
			HostSettings->NumPrivateConnections = 100;
			HostSettings->bAllowInvites = true;
			HostSettings->bAllowJoinInProgress = true;
			HostSettings->bShouldAdvertise = true;
			HostSettings->bAllowJoinViaPresence = true;
			HostSettings->bAllowJoinViaPresenceFriendsOnly = false;




			//HostSettings->Set(SETTING_GAMEMODE, GameType, EOnlineDataAdvertisementType::ViaOnlineService);
			//HostSettings->Set(SETTING_MAPNAME, MapName, EOnlineDataAdvertisementType::ViaOnlineService);
			//HostSettings->Set(SETTING_MATCHING_HOPPER, FString("TeamDeathmatch"), EOnlineDataAdvertisementType::DontAdvertise);
			//HostSettings->Set(SETTING_MATCHING_TIMEOUT, 120.0f, EOnlineDataAdvertisementType::ViaOnlineService);
			//HostSettings->Set(SETTING_SESSION_TEMPLATE_NAME, FString("GameSession"), EOnlineDataAdvertisementType::DontAdvertise);

#if !PLATFORM_SWITCH
			// On Switch, we don't have room for this in the session data (and it's not used anyway when searching), so there's no need to add it.
			// Can be readded if the buffer size increases.
			//	HostSettings->Set(SEARCH_KEYWORDS, CustomMatchKeyword, EOnlineDataAdvertisementType::ViaOnlineService);
#endif

			//	OnCreateSessionCompleteDelegateHandle = Sessions->AddOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegate);

			print("NO USERID SI CREATINGWITH 0!!!!!!!!!!!!!!!!!!!!!!!!!")
				return Sessions->CreateSession(0, InSessionName, *HostSettings);


		}
	}

	return false;
}


