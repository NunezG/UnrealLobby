// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UnrealLobby : ModuleRules
{
	public UnrealLobby(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		 PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "OnlineSubsystem", "HeadMountedDisplay", "UMG", "MediaAssets", "OnlineSubsystemNull", "OnlineSubsystemUtils", "Lobby", "Voice",  "Networking", "Sockets", "GameplayTags" });
        PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore", "OnlineSubsystem", "OnlineSubsystemUtils", "Sockets", "Networking", "Lobby" });
	}
}
