// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealLobbyCharacterBase.h"
#include "MyGameInstance.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/PlayerController.h"
#include "Engine/LocalPlayer.h"

#include "UnrealLobbyGameMode.h"

#include "LobbyBeaconState.h"
#include "Online/MyLobbyBeaconClient.h"
#include "LobbyBeaconGameMode.h"
#include "Online.h"
#include "CoreOnline.h"






// Sets default values
AUnrealLobbyCharacterBase::AUnrealLobbyCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AUnrealLobbyCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	

	print("InitBaseInitBaseInitB IN charactarrrrr");

	for (auto& Definition : GEngine->NetDriverDefinitions)
	{
		if (Definition.DefName == NAME_BeaconNetDriver)
		{
			UE_LOG(LogBeacon, Warning, TEXT("BEACONDRIVERDEFINITION IS IN ENGINE!!!!"));
			return;
		}
	}
	UE_LOG(LogBeacon, Warning, TEXT("ADDING BEACONDRIVERDEFINITION TO ENGINE!!!!"));


	FNetDriverDefinition BeaconDriverDefinition;
	BeaconDriverDefinition.DefName = NAME_BeaconNetDriver;
	BeaconDriverDefinition.DriverClassName = FName("/Script/OnlineSubsystemUtils.OnlineBeaconHost");
	BeaconDriverDefinition.DriverClassNameFallback = FName("/Script/OnlineSubsystemUtils.IpNetDriver");
	GEngine->NetDriverDefinitions.Add(BeaconDriverDefinition);
}

// Called every frame
void AUnrealLobbyCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AUnrealLobbyCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}







void AUnrealLobbyCharacterBase::HostParty()
{

	print("HostParty::::::: ");


	///////////////////////////////////////////////////////////////////////////

	//print_param(" SERVER RECEIVING HOST APRTY  %s ", *GetUniqueID());

	//FString Address = FString::Printf(TEXT("127.0.0.1:7777?Param1=%s?Param2=%s"), *Param1, *Param2);

	//UMyGameInstance* GI = Cast<UMyGameInstance>(GetGameInstance());
	//	FString const StartURL = FString::Printf(TEXT("/Game/Maps/MultiplayerTest/%s?game=%s%s%s?%s=%d%s"), "LobbyMap" /**GetMapName()*/, TEXT(""), TEXT("?listen") /*TEXT("")*/, TEXT("?bIsLanMatch") /*TEXT("")*/, TEXT(""), 0, TEXT(""));
	/*
	if (GI)
	{
	print("in  ");

	// Game instance will handle success, failure and dialogs
	bool result = Cast<UMyGameInstance>(GetGameInstance())->HostGame(GetGameInstance()->GetLocalPlayerByIndex(0)->GetPreferredUniqueNetId(), NAME_PartySession, StartURL);
	print("out ");

	}
	*/
	//print_param("SENDING HOST APRTY TO SERVER %s ", *GetGameInstance()->GetLocalPlayerByIndex(0)->GetPreferredUniqueNetId()->ToString());


	//if (Role == ROLE_Authority)
	//{


	//	}

	ServerHostParty();

}


void AUnrealLobbyCharacterBase::FindSessions()
{
	print("FINDING SESSIONS ");


	auto UserId = ((APlayerController*)Controller)->GetLocalPlayer()->GetPreferredUniqueNetId();

	UMyGameInstance* GameInstance = ((UMyGameInstance*)GetGameInstance());

	if (UserId->IsValid())
		print_param("FindSessionsFindSessions ID %s", *UserId->ToString());


	if(GameInstance)	
		GameInstance->FindSessions(UserId, false, false);

}


bool AUnrealLobbyCharacterBase::ServerHostParty_Validate()
{
	return true;
}


void AUnrealLobbyCharacterBase::ServerHostParty_Implementation()
{
	



	GetWorld()->GetAuthGameMode<ALobbyBeaconGameMode>()->StartPartyHost();

}


bool AUnrealLobbyCharacterBase::ServerHostLobby_Validate()
{
	return true;
}


void AUnrealLobbyCharacterBase::ServerHostLobby_Implementation()
{


	ALobbyBeaconGameMode* gamemm = GetWorld()->GetAuthGameMode<ALobbyBeaconGameMode>();

	auto autogamemm = GetWorld()->GetAuthGameMode();

	


		if (!autogamemm)
		{

			print("NONORMAL GAMEMODE EITHERRRRRR HOSTING LOBBYBYBYBYBYBYBBYBYBY!");
			return;
		}



	if (!gamemm)
	{

		print("NOGAMEMODE HOSTING LOBBYBYBYBYBYBYBBYBYBY!");
		return;
	}

	gamemm->HostLobby();

}


void AUnrealLobbyCharacterBase::HostLobby()
{
	print("HOST LOBBY CLIENT!!!!");

	ServerHostLobby();

}

void AUnrealLobbyCharacterBase::RequestLobbyReservation()
{
	print("RequestLobbyReservation!!!!");

	if (IsLocallyControlled())
	{
		if (((UMyGameInstance*)GetGameInstance())->GetSearchResults().Num() > 0)
		{
			print("CONNECTING");

			LobbyClient = GetWorld()->SpawnActor<AMyLobbyBeaconClient>(AMyLobbyBeaconClient::StaticClass());

			print_param("T�YPE BEFORE CONNECTION!!!! %s ", *LobbyClient->GetBeaconType());


	//		TSharedPtr<FOnlineSessionInfoNull> SessionInfo = StaticCastSharedPtr<FOnlineSessionInfoNull>((((UMyGameInstance*)GetGameInstance())->GetSearchResults()[0].Session.SessionInfo));

			//	FOnlineSessionInfo& ssinfo = *(((UMyGameInstance*)GetGameInstance())->GetSearchResults()[0].Session.SessionInfo);
			//	ssinfo
		//	SessionInfo->

				LobbyClient->ConnectToLobby(((UMyGameInstance*)GetGameInstance())->GetSearchResults()[0]);


			//	(((UMyGameInstance*)GetGameInstance())->GetSearchResults()[0]).Session.SessionInfo
			print_param("T�YPE AFTERC CONNECTION!!!! %s ", *LobbyClient->GetBeaconType());


			ALobbyBeaconState* LobbyState = LobbyClient->LobbyState;

			//LobbyState->

			/*
			//must be logged in

			LobbyClient->ClientJoinGame();

			//	LobbyClient->log
			LobbyClient->JoiningServer();

			*/



			//LobbyClient->OnConnected();
			//	RequestReservation(((UMyGameInstance*)GetGameInstance())->GetSearchResults()[0], userID /*PlayerState->UniqueId.GetUniqueNetId()*/, PartyMembers);
			print("RequestReservatuon END en player");

		}
		else
		{
			print("NO SEARCH RESULT!!!");

		}




		//TSharedPtr<const FUniqueNetId> UserId;

		//PartyClient->connect

		//GetPreferredUniqueNetId();


		//FUniqueNetId dfsgsdq;
		//*UserId = GetUniqueID();

		//ServerRequestPartyReservation();

		/*	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

		IOnlineIdentityPtr identity = nullptr;

		TSharedPtr<const FUniqueNetId> userID = nullptr;

		if (OnlineSub)
		{
		identity = OnlineSub->GetIdentityInterface();
		}

		if (identity.IsValid())
		{
		userID = identity->GetUniquePlayerId(0);

		}
		*/

		ServerRequestLobbyReservation();


		//PartyClient->RequestReservation("192.168.1.1:15000//Game/Maps/MultiplayerTest/LobbyMap", nameString.ToString(), PlayerState->UniqueId.GetUniqueNetId(), PartyMembers);
	}
	else
	{

		print("TRYING TO REGISTER BEACON IN SERVER!!!!!!!!!!!!!!!!!!!!!");
	}
}

bool  AUnrealLobbyCharacterBase::ServerRequestLobbyReservation_Validate()
{

	return true;

}


void  AUnrealLobbyCharacterBase::ServerRequestLobbyReservation_Implementation()
{
	//FUniqueNetIdRepl repp = FUniqueNetIdRepl(&GetUniqueID());
	//res.UniqueId = userID;


	//FName nameString = NAME_PartySession;

	//	AMyGameSession* const GameSession = ((UMyGameInstance*)GetGameInstance())->GetGameSession();

	//	if (!GameSession)
	//	{
	//	print("NO GAMESESISISISION!");
	//
	//	return;
	//}

	//print_param("RequestReservatuon %i", GameSession->GetSearchResults().Num());

	//7787
	//GetSearchResults()[SessionIndexInSearchResults]


}



void AUnrealLobbyCharacterBase::RequestPartyReservation()
{
	if (IsLocallyControlled())
	{
		//TSharedPtr<const FUniqueNetId> UserId;

		//PartyClient->connect

		//GetPreferredUniqueNetId();


		//FUniqueNetId dfsgsdq;
		//*UserId = GetUniqueID();

		//ServerRequestPartyReservation();





		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

		IOnlineIdentityPtr identity = nullptr;

		TSharedPtr<const FUniqueNetId> userID = nullptr;

		if (OnlineSub)
		{
			identity = OnlineSub->GetIdentityInterface();
		}

		if (identity.IsValid())
		{
			userID = identity->GetUniquePlayerId(0);

		}


		print("CRETING PARTY client");

		FPlayerReservation reservation;
		{
			reservation.UniqueId = userID;
			reservation.ValidationStr = "FUCKYOU";
		}


		//FUniqueNetIdRepl repp = FUniqueNetIdRepl(&GetUniqueID());
		//res.UniqueId = userID;


		//FName nameString = NAME_PartySession;

		TArray<FPlayerReservation> PartyMembers;

		PartyMembers.Add(reservation);



		//	AMyGameSession* const GameSession = ((UMyGameInstance*)GetGameInstance())->GetGameSession();

		//	if (!GameSession)
		//	{
		//	print("NO GAMESESISISISION!");
		//
		//	return;
		//}

		//print_param("RequestReservatuon %i", GameSession->GetSearchResults().Num());

		//7787
		//GetSearchResults()[SessionIndexInSearchResults]
		if (((UMyGameInstance*)GetGameInstance())->GetSearchResults().Num() > 0)
		{
			PartyClient = GetWorld()->SpawnActor<AMyPartyBeaconClient>(AMyPartyBeaconClient::StaticClass());
			PartyClient->SetOwner(this);
			PartyClient->Instigator = this;

			PartyClient->RequestReservation(((UMyGameInstance*)GetGameInstance())->GetSearchResults()[0], userID /*PlayerState->UniqueId.GetUniqueNetId()*/, PartyMembers);
			print("RequestReservatuon OK");

		}
		else
		{
			print("NO SEARCH RESULT!!!");

		}
		//PartyClient->RequestReservation("192.168.1.1:15000//Game/Maps/MultiplayerTest/LobbyMap", nameString.ToString(), PlayerState->UniqueId.GetUniqueNetId(), PartyMembers);
	}
	else
	{

		print("TRYING TO REGISTER BEACON IN SERVER!!!!!!!!!!!!!!!!!!!!!");

	}
}

bool  AUnrealLobbyCharacterBase::ServerRequestPartyReservation_Validate()
{

	return true;

}


void  AUnrealLobbyCharacterBase::ServerRequestPartyReservation_Implementation()
{


}



void AUnrealLobbyCharacterBase::ActorTalk()
{
	print("ACGORT ALK");
	if (HasAuthority())
	{
		print("THE FUCHING ATOR IS TALCKING IN SERVERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");


	}else 		print("THE FUCHING ATOR IS TALCKING IN CLIENTTTTTTT");


}


void AUnrealLobbyCharacterBase::CreateRoom()
{


	ServerHostParty();

}
