// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/GameMode.h"
//#include "GameFramework/PlayerStart.h"

#include "Online/MyPartyBeaconHost.h"

#include "Online/MyOnlineBeaconHost.h"

#include "Online/MyLobbyBeaconHostObject.h"

#include "UnrealLobbyGameMode.generated.h"

UCLASS(minimalapi)
class AUnrealLobbyGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AUnrealLobbyGameMode();


//	virtual void PostLogin(APlayerController* NewPlayer) override;

private:

public:

	virtual void BeginPlay() override;

	/** finish current match and lock players */
//	UFUNCTION(exec)
	//	void FinishMatch();

//	UFUNCTION(BlueprintCallable)
	//	virtual void SceneUpdate(); // Request an update of the state of the scene in the server (media players time...)

									/*Finishes the match and bumps everyone to main menu.*/
									/*Only GameInstance should call this function */
//	void RequestFinishAndExitToMainMenu();



};



