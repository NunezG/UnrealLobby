// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UnrealLobby.h"
#include "PartyBeaconClient.h"
#include "MyPartyBeaconClient.generated.h"

/**
 * 
 */
UCLASS()
class  AMyPartyBeaconClient : public APartyBeaconClient
{
	GENERATED_UCLASS_BODY()
	
public:

	/** Common initialization for all beacon types */
	virtual bool InitBase() override;

		//~ Begin UObject Interface
		virtual void BeginDestroy() override;
	//~ End UObject Interface

	//~ Begin AOnlineBeaconClient Interface
	virtual void OnConnected() override;
	virtual void OnFailure() override;
	//~ End AOnlineBeaconClient Interface

	/** Send a ping RPC to the client */
	UFUNCTION(client, reliable)
		virtual void ClientPing();

	/** Send a pong RPC to the host */
	UFUNCTION(server, reliable, WithValidation)
		virtual void ServerPong();


	/**
	* Sends a request to the remote host to allow the specified members to reserve space
	* in the host's session. Note this request is async.
	*
	* @param ConnectInfoStr the URL of the server that the connection will be made to
	* @param InSessionId Id of the session expected to be found at this destination
	* @param RequestingPartyLeader the leader of this party that will be joining
	* @param PartyMembers the list of players that want to reserve space
	*
	* @return true if the request able to be sent, false if it failed to send
	*/
	virtual bool RequestReservation(const FString& ConnectInfoStr, const FString& InSessionId, const FUniqueNetIdRepl& RequestingPartyLeader, const TArray<FPlayerReservation>& PartyMembers) override;

	/**
	* Sends a request to the remote host to allow the specified members to reserve space
	* in the host's session. Note this request is async.
	*
	* @param DesiredHost a search result describing the server that the connection will be made to
	* @param RequestingPartyLeader the leader of this party that will be joining
	* @param PartyMembers the list of players that want to reserve space
	*
	* @return true if the request able to be sent, false if it failed to send
	*/
	virtual bool RequestReservation(const FOnlineSessionSearchResult& DesiredHost, const FUniqueNetIdRepl& RequestingPartyLeader, const TArray<FPlayerReservation>& PartyMembers) override;

	/**
	* Sends an update request to the remote host to append additional members to an existing party
	* in the host's session. Note this request is async.
	*
	*  ** This version is for existing / established connections only, it will not actually attempt a connection **
	*
	* @param RequestingPartyLeader the leader of the party that will be updated
	* @param PlayersToAdd the list of players that will be added to the party
	*
	* @return true if the request able to be sent, false if it failed to send
	*/
	virtual bool RequestReservationUpdate(const FUniqueNetIdRepl& RequestingPartyLeader, const TArray<FPlayerReservation>& PlayersToAdd) override;


	/**
	* Tell the server about the reservation request being made
	*
	* @param SessionId expected session id on the other end (must match)
	* @param Reservation pending reservation request to make with server
	*/
		virtual void ServerReservationRequest(const FString& SessionId, const FPartyReservation& Reservation) override;

	/**
	* Response from the host session after making a reservation request
	*
	* @param ReservationResponse response from server
	*/
		virtual void ClientReservationResponse(EPartyReservationResult::Type ReservationResponse) override;

	/** Response from the host session that the reservation is full */
		virtual void ClientSendReservationFull() override;


		void MakeActorTalk();
		

};
