// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LobbyBeaconClient.h"
#include "MyLobbyBeaconClient.generated.h"

/**
 * 
 */
UCLASS()
class UNREALLOBBY_API AMyLobbyBeaconClient : public ALobbyBeaconClient
{
	GENERATED_BODY()
	

		//~ Begin AOnlineBeaconClient Interface
		virtual void OnConnected() override;
	//~ End AOnlineBeaconClient Interface
	
	/**
	* Internal function to log in a local players when first connected to the beacon
	*/
	virtual void LoginLocalPlayers();


	/**
	* Client notification result for a single login attempt
	*
	* @param InUniqueId id of player involved
	* @param bWasSuccessful result of the login attempt
	*/
	//UFUNCTION(client, reliable)
		void ClientLoginComplete_Implementation(const FUniqueNetIdRepl& InUniqueId, bool bWasSuccessful);


	/**
	* Client notification that another player has joined the lobby
	*
	* @param NewPlayerName display name of new player
	* @param InUniqueId unique id of new player
	*/
	//UFUNCTION(client, reliable)
		virtual void ClientPlayerJoined_Implementation(const FText& NewPlayerName, const FUniqueNetIdRepl& InUniqueId);
};
