// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LobbyBeaconHost.h"
#include "MyLobbyBeaconHostObject.generated.h"

/**
 * 
 */
UCLASS()
class  AMyLobbyBeaconHostObject : public ALobbyBeaconHost
{
	GENERATED_UCLASS_BODY()
	
public:

	/**
	* Initialize the lobby beacon, creating an object to maintain state
	*
	* @param InSessionName name of session the beacon is associated with
	*/
	virtual bool Init(FName InSessionName) override;

	/**
	* Create the lobby game state and associate it with the game
	*
	* @param InMaxPlayers max number of players allowed in the lobby
	*/
	virtual void SetupLobbyState(int32 InMaxPlayers) override;

	/**
	* Handle a detected disconnect of an existing player on the server
	*
	* @param InUniqueId unique id of the player
	*/
	virtual void HandlePlayerLogout(const FUniqueNetIdRepl& InUniqueId) override;

	/**
	* Tell all connected beacon clients about the current joinability settings
	*
	* @param Setting current joinability settings
	*/
	virtual void AdvertiseSessionJoinability(const FJoinabilitySettings& Settings) override;



	virtual bool PreLogin(const FUniqueNetIdRepl& InUniqueId, const FString& Options) override;

	/**
	* Notification call that a new lobby connection has been successfully establish
	*
	* @param ClientActor new lobby client connection
	*/
	virtual void PostLogin(ALobbyBeaconClient* ClientActor) override;

	
	
};
