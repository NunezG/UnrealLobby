// Fill out your copyright notice in the Description page of Project Settings.

#include "MyOnlineBeaconHost.h"
#include "UnrealLobby.h"




AMyOnlineBeaconHost::AMyOnlineBeaconHost(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	//Set the beacon host state to allow requests
	BeaconState = EBeaconState::AllowRequests;
}

bool AMyOnlineBeaconHost::Start()
{
	print("STARTING BEACON HOST");

	//Call our init to start up the network interface
	IsReady = InitHost();
	print_param("GETTING BEACON HOST READY %i", IsReady);

	return IsReady;
}

void AMyOnlineBeaconHost::AddHost(AOnlineBeaconHostObject* HostObject)
{


	/** Make sure we inited properly */
	if (IsReady)
	{
		print("ADDING A HOST");

		RegisterHost(HostObject);
	}
	else
	{
		print("ONLINEBEACONHOST IS NOT READY");


	}
}