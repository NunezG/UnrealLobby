// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PartyBeaconHost.h"
#include "MyPartyBeaconHost.generated.h"

/**
 * 
 */
UCLASS()
class  AMyPartyBeaconHost : public APartyBeaconHost
{
	GENERATED_UCLASS_BODY()
	
public:

	/** Common initialization for all beacon types */
	//virtual bool InitBase() override;

	//~ Begin UObject interface
	virtual void PostInitProperties() override;
	//~ End UObject interface

	//~ Begin AActor Interface
	virtual void Tick(float DeltaTime) override;
	
	virtual bool Init();

	//~ Begin AOnlineBeaconHost Interface 
	virtual AOnlineBeaconClient* SpawnBeaconActor(class UNetConnection* ClientConnection) override;
	virtual void OnClientConnected(class AOnlineBeaconClient* NewClientActor, class UNetConnection* ClientConnection) override;
	//~ End AOnlineBeaconHost Interface 


	/**
	* Initialize the party host beacon
	*
	* @param InTeamCount number of teams to make room for
	* @param InTeamSize size of each team
	* @param InMaxReservation max number of reservations allowed
	* @param InSessionName name of session related to the beacon
	* @param InForceTeamNum team to force players on if applicable (usually only 1 team games)
	*
	* @return true if successful created, false otherwise
	*/
	virtual bool InitHostBeacon(int32 InTeamCount, int32 InTeamSize, int32 InMaxReservations, FName InSessionName, int32 InForceTeamNum = 0) override;


	/**
	* Attempts to add a party reservation to the beacon
	*
	* @param ReservationRequest reservation attempt
	*
	* @return add attempt result
	*/
	virtual EPartyReservationResult::Type AddPartyReservation(const FPartyReservation& ReservationRequest) override;


	/**
	* Update party leader for a given player with the reservation beacon
	* (needed when party leader leaves, reservation beacon is in a temp/bad state until someone updates this)
	*
	* @param InPartyMemberId party member making the update
	* @param NewPartyLeaderId id of new leader
	*/
	virtual void UpdatePartyLeader(const FUniqueNetIdRepl& InPartyMemberId, const FUniqueNetIdRepl& NewPartyLeaderId) override;



	/**
	* Handle a reservation request received from an incoming client
	*
	* @param Client client beacon making the request
	* @param SessionId id of the session that is being checked
	* @param ReservationRequest payload of request
	*/
	virtual void ProcessReservationRequest(APartyBeaconClient* Client, const FString& SessionId, const FPartyReservation& ReservationRequest) override;

};
