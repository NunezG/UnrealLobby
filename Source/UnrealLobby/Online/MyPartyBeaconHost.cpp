// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPartyBeaconHost.h"
#include "OnlineSubsystemTypes.h"
#include "OnlineSubsystemUtils.h"
#include "MyPartyBeaconClient.h"

AMyPartyBeaconHost::AMyPartyBeaconHost(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	ClientBeaconActorClass = AMyPartyBeaconClient::StaticClass();
	BeaconTypeName = ClientBeaconActorClass->GetName();
}


bool AMyPartyBeaconHost::Init()
{
	UE_LOG(LogBeacon, Verbose, TEXT("Init PARTY HOST"));

	return true;
}

//~ Begin UObject interface
void AMyPartyBeaconHost::PostInitProperties()
{
	Super::PostInitProperties();

}
//~ End UObject interface

//~ Begin AActor Interface
void AMyPartyBeaconHost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

/**
* Initialize the party host beacon
*
* @param InTeamCount number of teams to make room for
* @param InTeamSize size of each team
* @param InMaxReservation max number of reservations allowed
* @param InSessionName name of session related to the beacon
* @param InForceTeamNum team to force players on if applicable (usually only 1 team games)
*
* @return true if successful created, false otherwise
*/
bool AMyPartyBeaconHost::InitHostBeacon(int32 InTeamCount, int32 InTeamSize, int32 InMaxReservations, FName InSessionName, int32 InForceTeamNum)
{
	UE_LOG(LogBeacon, Warning, TEXT("InitHostBeacon!!!!"));


	return Super::InitHostBeacon(InTeamCount, InTeamSize, InMaxReservations, InSessionName, InForceTeamNum);


}

EPartyReservationResult::Type AMyPartyBeaconHost::AddPartyReservation(const FPartyReservation& ReservationRequest)
{
	UE_LOG(LogBeacon, Warning, TEXT("AddPartyReservation!!!!"));

	return Super::AddPartyReservation(ReservationRequest);
}


void AMyPartyBeaconHost::UpdatePartyLeader(const FUniqueNetIdRepl& InPartyMemberId, const FUniqueNetIdRepl& NewPartyLeaderId)
{
	UE_LOG(LogBeacon, Warning, TEXT("UpdatePartyLeader!!!!"));

	Super::UpdatePartyLeader(InPartyMemberId, NewPartyLeaderId);

}


void AMyPartyBeaconHost::ProcessReservationRequest(APartyBeaconClient* Client, const FString& SessionId, const FPartyReservation& ReservationRequest)
{
	UE_LOG(LogBeacon, Warning, TEXT("ProcessReservationRequest SESSIONID!!!! %s"), *SessionId);

	if (State)
	{
		UWorld* World = GetWorld();
		FName SessionName = State->GetSessionName();


		UE_LOG(LogBeacon, Warning, TEXT(" SessionName!!!! %s"), *SessionName.ToString());

		IOnlineSessionPtr SessionInt = Online::GetSessionInterface(World);

		if (SessionInt.IsValid())
		{

			UE_LOG(LogBeacon, Warning, TEXT("sessionintok: %i "), SessionInt->GetNumSessions());
		}
		
		FNamedOnlineSession* Session = SessionInt.IsValid() ? SessionInt->GetNamedSession(SessionName) : NULL;
		
	/*	for (int32 SearchIndex = 0; SearchIndex <  Sessions.Num(); SearchIndex++)
		{
			if (Sessions[SearchIndex].SessionName == SessionName)
			{
				return &Sessions[SearchIndex];
			}
		}
*/
		//UE_LOG(LogBeacon, Warning, TEXT("SessionNameSessionNameSessionName: %s"), *SessionName.ToString());
		if (Session)
		{
			UE_LOG(LogBeacon, Warning, TEXT("HASSESSIONNNN!!!!"));
			if (Session->SessionInfo.IsValid())
			{
				UE_LOG(LogBeacon, Warning, TEXT("HASSESSIONNNNINFOFOFOFOFO!!!!"));

			}


			if (Session->SessionInfo.IsValid())
			{
				UE_LOG(LogBeacon, Warning, TEXT("GetSessionIdGetSessionIdGetSessionId: %s"), *Session->SessionInfo->GetSessionId().ToString());
			}
			else
			{
				UE_LOG(LogBeacon, Warning, TEXT("NO SESSIOSessionInfoSessionInfoSessionInfoSessionInfoNNN!!!!"));

			}
		}
		else
		{
			UE_LOG(LogBeacon, Warning, TEXT("NO SESSIONNNNNNNNNNNNNNNNNNNNNNN!!!!"));

		}

		Super::ProcessReservationRequest(Client, SessionId, ReservationRequest);
	
	}
	//print_param("PartyHost->GetState()->GetNumConsumedReservations() ENDNDDD; %i", GetState()->GetNumConsumedReservations());
	//print_param("		PartyHost->GetState()->GetReservationCount())ENDNDDDENDNDDD; %i", GetState()->GetReservationCount());
}


void AMyPartyBeaconHost::OnClientConnected(AOnlineBeaconClient* NewClientActor, UNetConnection* ClientConnection)
{
	UE_LOG(LogBeacon, Warning, TEXT("OnClientConnected response in server!!!!"));

	Super::OnClientConnected(NewClientActor, ClientConnection);

	AMyPartyBeaconClient* BeaconClient = Cast<AMyPartyBeaconClient>(NewClientActor);
	if (BeaconClient != NULL)
	{
		BeaconClient->ClientPing();

		BeaconClient->MakeActorTalk();
	}

}

AOnlineBeaconClient* AMyPartyBeaconHost::SpawnBeaconActor(UNetConnection* ClientConnection)
{
	UE_LOG(LogBeacon, Verbose, TEXT("SpawnBeaconActor!!!!"));

	return Super::SpawnBeaconActor(ClientConnection);
}
