// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LobbyBeaconState.h"
#include "MyLobbyBeaconState.generated.h"

/**
 * 
 */
UCLASS()
class  AMyLobbyBeaconState : public ALobbyBeaconState
{
	GENERATED_BODY()
	

		/**
		* Start the waiting for other players (first player logged in success)
		*/
		virtual void StartWaiting() override;
	
		/**
		* Create a new player for this lobby beacon state
		*
		* @param PlayerName name of the player
		* @param UniqueId uniqueid of the player
		*
		* @return new player state actor or nullptr if the player should be disconnected
		*/
		virtual ALobbyBeaconPlayerState* CreateNewPlayer(const FText& PlayerName, const FUniqueNetIdRepl& UniqueId) override;
	
};
