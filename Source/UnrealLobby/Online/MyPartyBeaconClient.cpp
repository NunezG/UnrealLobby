// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPartyBeaconClient.h"
#include "Kismet/GameplayStatics.h"
#include "UnrealLobbyCharacter.h"


AMyPartyBeaconClient::AMyPartyBeaconClient(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	

}

void AMyPartyBeaconClient::BeginDestroy()
{
	Super::BeginDestroy();

}

void AMyPartyBeaconClient::OnConnected()
{
	UE_LOG(LogBeacon, Warning, TEXT("OnConnectedOnConnectedOnConnectedOnConnectedOnConnectedOnConnectedOnConnected"));

	Super::OnConnected();
	//ClientPing();
}

void AMyPartyBeaconClient::OnFailure()
{
#if !UE_BUILD_SHIPPING
	UE_LOG(LogBeacon, Verbose, TEXT("Test beacon connection failure, handling connection timeout."));
#endif
	Super::OnFailure();

}

void AMyPartyBeaconClient::ClientPing_Implementation()
{
	UE_LOG(LogBeacon, Log, TEXT("Ping"));
	ServerPong();

	AOnlineBeaconHostObject* owner = GetBeaconOwner();

	APartyBeaconHost* BeaconHost = (APartyBeaconHost*)owner;
	if (BeaconHost)
	{
		//CALL IN HOST
	}

}

bool AMyPartyBeaconClient::ServerPong_Validate()
{
#if !UE_BUILD_SHIPPING
	return true;
#else
	return false;
#endif
}

void AMyPartyBeaconClient::ServerPong_Implementation()
{
	UE_LOG(LogBeacon, Log, TEXT("Pong!!!!!!!!!!!!!!!!!!"));
	//ClientPing();

}

bool AMyPartyBeaconClient::RequestReservation(const FString& ConnectInfoStr, const FString& InSessionId, const FUniqueNetIdRepl& RequestingPartyLeader, const TArray<FPlayerReservation>& PartyMembers)
{
	print_param("WE ARE REQUESTING RESRVATION INFORMATION 111 %s", *InSessionId);
	print_param("ConnectInfoStrConnectInfoStrConnectInfoStr %s", *ConnectInfoStr);

	PendingReservation.TeamNum = 1;

	bool res = Super::RequestReservation(ConnectInfoStr, InSessionId,  RequestingPartyLeader, PartyMembers);
	print_param("result %i", res);

	if (PendingReservation.IsValid())
	{
		print("RESERVATION 111111 VALIDDDDDD");


	}
	else {
		print_param("RESERVATION111 1 VALIDDDDDD %s", *PendingReservation.PartyLeader.ToString());
		print_param("RESERVATION111 2 VALIDDDDDD %i", PendingReservation.PartyMembers.Num());
		print_param("RESERVATION111 3 VALIDDDDDD %i", PendingReservation.TeamNum);
		PendingReservation.Dump();
		print("//////////////////////////////////////////////////////////////////////////");



		if (PendingReservation.PartyLeader.IsValid() && PartyMembers.Num() >= 1)
		{
			//bIsValid = true;
			for (const FPlayerReservation& PlayerRes : PartyMembers)
			{
				if (!PlayerRes.UniqueId.IsValid())
				{
					print_param("PlayerRes NO VALID11111! %s", *PlayerRes.UniqueId.ToString());

					//bIsValid = false;
					break;
				}

				if (PendingReservation.PartyLeader == PlayerRes.UniqueId &&
					PlayerRes.ValidationStr.IsEmpty())
				{
					print_param("PendingReservation.PartyLeader == PlayerRes.UniqueId &&PlayerRes.ValidationStr.IsEmpty!! %s", *PlayerRes.ValidationStr);

					//bIsValid = false;
					break;
				}
			}
		}
		else
		{
			if (PendingReservation.PartyLeader.IsValid())
			{
				print_param("PARTY LEADER NO VALID11111! %s", *PendingReservation.PartyLeader.ToString());
			}
		}




	}
	return res;

}

/**
* Sends a request to the remote host to allow the specified members to reserve space
* in the host's session. Note this request is async.
*
* @param DesiredHost a search result describing the server that the connection will be made to
* @param RequestingPartyLeader the leader of this party that will be joining
* @param PartyMembers the list of players that want to reserve space
*
* @return true if the request able to be sent, false if it failed to send
*/
bool AMyPartyBeaconClient::RequestReservation(const FOnlineSessionSearchResult& DesiredHost, const FUniqueNetIdRepl& RequestingPartyLeader, const TArray<FPlayerReservation>& PartyMembers)
{
	print("WE ARE REQUESTING RESRVATION INFORMATION 222");

	bool res = Super::RequestReservation(DesiredHost, RequestingPartyLeader, PartyMembers);

	if (PendingReservation.IsValid())
	{
		print("RESERVATION 1111VALIDDDDDD");


	}
	else {
		print("RESERVATION 2222NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO VALIDDDDDD");
	}

	return res;
}

/**
* Tell the server about the reservation request being made
*
* @param SessionId expected session id on the other end (must match)
* @param Reservation pending reservation request to make with server
*/
UFUNCTION(server, reliable, WithValidation)
void AMyPartyBeaconClient::ServerReservationRequest(const FString& SessionId, const FPartyReservation& Reservation)
{


	print_param("SENDINGRESERVATINO TO SERVER %s ", *SessionId);

	print("DUMPING " );

	Reservation.Dump();

	if (Reservation.IsValid())
	{
		print(" RESERVATIN yes VALID");

	}
	else
	{
		print("WTF RESERVATINOISNOT VALID");

	}

	Super::ServerReservationRequest(SessionId, Reservation);


}

/**
* Response from the host session after making a reservation request
*
* @param ReservationResponse response from server
*/
UFUNCTION(client, reliable)
void AMyPartyBeaconClient::ClientReservationResponse(EPartyReservationResult::Type ReservationResponse)
{
	print("ClientReservationResponse!!!!!!!!!!!!!!!!!!!!!!!!!!!");


	Super::ClientReservationResponse(ReservationResponse);

}

/** Response from the host session that the reservation is full */
UFUNCTION(client, reliable)
void AMyPartyBeaconClient::ClientSendReservationFull()
{
	print("ClientReservationRFULLLLLLLLLLLLLLLLLLL");

	Super::ClientSendReservationFull();

}


bool AMyPartyBeaconClient::InitBase() 
{
	return Super::InitBase();

}

bool AMyPartyBeaconClient::RequestReservationUpdate(const FUniqueNetIdRepl& RequestingPartyLeader, const TArray<FPlayerReservation>& PlayersToAdd)
{

	return Super::RequestReservationUpdate(RequestingPartyLeader, PlayersToAdd);

}


void AMyPartyBeaconClient::MakeActorTalk()
{
	AUnrealLobbyCharacterBase* uBase = (AUnrealLobbyCharacterBase*)GetOwner();

	if (!uBase)
	{
		print("NO GetOwnerGetOwnerGetOwnerGetOwner!!!!");

	}else 	uBase->ActorTalk();


	AUnrealLobbyCharacterBase* uBasinste = (AUnrealLobbyCharacterBase*)GetInstigator();

	if (!uBasinste)
	{
		print("NO GetInstigatorGetInstigatorGetInstigatorGetInstigator!!!!");

	}else 	uBasinste->ActorTalk();

	
	AUnrealLobbyCharacterBase* uBasiworl = (AUnrealLobbyCharacterBase*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	if (!uBasiworl)
	{
		print("NO GetPlayerCharacterGetPlayerCharacterGetPlayerCharacter!!!!");

		return;
	}



	print("NMAKING ACTOR TALKKK!!!!!!!!!!!!!!!!");


	uBasiworl->ActorTalk();


}
