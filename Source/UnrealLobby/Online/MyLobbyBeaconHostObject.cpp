// Fill out your copyright notice in the Description page of Project Settings.

#include "MyLobbyBeaconHostObject.h"
#include "MyLobbyBeaconState.h"
#include "MyLobbyBeaconClient.h"
#include "LobbyBeaconClient.h"
#include "UnrealLobby.h"
#include "GameFramework/OnlineReplStructs.h"



AMyLobbyBeaconHostObject::AMyLobbyBeaconHostObject(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	//Set the beacon host state to allow requests
	LobbyStateClass = AMyLobbyBeaconState::StaticClass();
	ClientBeaconActorClass = AMyLobbyBeaconClient::StaticClass();
}



bool AMyLobbyBeaconHostObject::Init(FName InSessionName)
{

	UE_LOG(LogBeacon, Warning, TEXT("INIT LOBBY BEACON HOST!! %s"), *InSessionName.ToString());

return 	Super::Init(InSessionName);

}


void AMyLobbyBeaconHostObject::SetupLobbyState(int32 InMaxPlayers)
{
	UE_LOG(LogBeacon, Warning, TEXT("SetupLobbyState in LOBBY BEACON HOST!! %i"), InMaxPlayers);

	Super::SetupLobbyState(InMaxPlayers);

}



void AMyLobbyBeaconHostObject::HandlePlayerLogout(const FUniqueNetIdRepl& InUniqueId)
{
	//*InUniqueId->


	UE_LOG(LogBeacon, Warning, TEXT("HandlePlayerLogout in LOBBY BEACON HOST!! %s"), *(*InUniqueId->ToString()));

	Super::HandlePlayerLogout(InUniqueId);

}



void AMyLobbyBeaconHostObject::AdvertiseSessionJoinability(const FJoinabilitySettings& Settings)
{
	UE_LOG(LogBeacon, Warning, TEXT("AdvertiseSessionJoinability LOBBY BEACON HOST!!"));

	Super::AdvertiseSessionJoinability(Settings);

}




bool AMyLobbyBeaconHostObject::PreLogin(const FUniqueNetIdRepl& InUniqueId, const FString& Options)
{
	UE_LOG(LogBeacon, Warning, TEXT("PreLogin LOBBY BEACON HOST!! %s"), *Options);

	return Super::PreLogin(InUniqueId, Options);

}


void AMyLobbyBeaconHostObject::PostLogin(ALobbyBeaconClient* ClientActor)
{
	print_param("PostLogin LOBBY BEACON HOST!! %s ", *ClientActor->GetName());


	Super::PostLogin(ClientActor);

}

