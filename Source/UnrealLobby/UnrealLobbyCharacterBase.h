// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UnrealLobby.h"
#include "GameFramework/Character.h"
#include "Online/MyPartyBeaconClient.h"
#include "LobbyBeaconClient.h"

#include "UnrealLobbyCharacterBase.generated.h"

UCLASS()
class UNREALLOBBY_API AUnrealLobbyCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AUnrealLobbyCharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	UFUNCTION(BlueprintCallable)
		void FindSessions();


	UFUNCTION(WithValidation, Server, Reliable, Category = "PartyBeacon")
		void ServerHostParty();


	UFUNCTION(WithValidation, Server, Reliable, Category = "LobbyBeacon")
		void ServerHostLobby();


	UFUNCTION(WithValidation, Server, Reliable, Category = "Use")
		void ServerRequestPartyReservation();

	UFUNCTION(WithValidation, Server, Reliable, Category = "Use")
		void ServerRequestLobbyReservation();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		AMyPartyBeaconClient* PartyClient;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ALobbyBeaconClient* LobbyClient;

	UFUNCTION(BlueprintCallable)
		void HostParty();

	UFUNCTION(BlueprintCallable)
		void HostLobby();

	UFUNCTION(BlueprintCallable)
		void RequestPartyReservation();

	UFUNCTION(BlueprintCallable)
		void RequestLobbyReservation();


	void ActorTalk();


	UFUNCTION(BlueprintCallable)
		void CreateRoom();

};
