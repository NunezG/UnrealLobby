// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#define print(text) if (GEngine){ GEngine->AddOnScreenDebugMessage(-1, 6, FColor::White, text); UE_LOG(LogTemp, Warning, TEXT(text));}
#define print_param(text, param) if (GEngine){ GEngine->AddOnScreenDebugMessage(-1, 6, FColor::White, FString::Printf(TEXT(text), param)); UE_LOG(LogTemp, Warning, TEXT(text), param);}
