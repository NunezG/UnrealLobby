// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "UnrealLobbyGameMode.h"
#include "UnrealLobbyCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUnrealLobbyGameMode::AUnrealLobbyGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}



void AUnrealLobbyGameMode::BeginPlay()
{
	Super::BeginPlay();


}
