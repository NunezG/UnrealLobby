// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
//#include "AdvancedFriendsGameInstance.h"
//#include "Online/MyGameSession.h"
#include "GameFramework/GameSession.h"
#include "OnlineSessionInterface.h"
#include "Online.h"
#include "OnlineSubsystemNull.h"
#include "UnrealLobby.h"

#include "MyGameInstance.generated.h"


//class AMyGameSession;
/**
 * 
 */
UCLASS()
class UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()

		UMyGameInstance(const FObjectInitializer& ObjectInitializer);
	 
public:

	/** Current search settings */
	TSharedPtr<class FOnlineSessionSearch> SearchSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool IsVRMode = true;


	//UFUNCTION(BlueprintCallable)
	//	bool NewHostGame(FUniqueNetIdRepl NetID, FName InSessionName, const FString& InTravelURL);

//	UFUNCTION(BlueprintCallable)
//		AMyGameSession* GetGameSession() const;

	//UFUNCTION(BlueprintCallable)
	//	TArray<struct FBlueprintSessionResult> getResultBPArray() const;

	/**
	* Joins one of the session in search results
	*
	* @param UserId user that initiated the request
	* @param SessionName name of session
	* @param SessionIndexInSearchResults Index of the session in search results
	*
	* @return bool true if successful, false otherwise
	*/
//	UFUNCTION(BlueprintCallable)
//	bool NewJoinSession(int32 SessionIndexInSearchResults);

	//UFUNCTION(BlueprintCallable)
	//	bool MyJoinSession(APlayerController* theController, const FBlueprintSessionResult& SearchResult);

	virtual bool JoinSession(ULocalPlayer* LocalPlayer, const FOnlineSessionSearchResult& SearchResult) override;


	
	/**
	* Joins one of the session in search results
	*
	* @param UserId user that initiated the request
	* @param SessionName name of session
	* @param SessionIndexInSearchResults Index of the session in search results
	*
	* @return bool true if successful, false otherwise
	*/
	UFUNCTION(BlueprintCallable)
		virtual bool JoinSession(ULocalPlayer* LocalPlayer, int32 SessionIndexInSearchResults) override;

	/** Initiates the session searching */
	bool FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsDedicatedServer, bool bLANMatch);

	/**
	* Get the search results.
	*
	* @return Search results
	*/
	const TArray<FOnlineSessionSearchResult> & GetSearchResults() const;
private:



	void AddNetworkFailureHandlers();

	/** A hard-coded encryption key used to try out the encryption code. This is NOT SECURE, do not use this technique in production! */
	TArray<uint8> DebugTestEncryptionKey;

	FDelegateHandle OnJoinSessionCompleteDelegateHandle;
	FDelegateHandle TravelLocalSessionFailureDelegateHandle;
	FDelegateHandle OnSearchSessionsCompleteDelegateHandle;

	/** Callback which is intended to be called upon joining session */
	void OnJoinSessionComplete(EOnJoinSessionCompleteResult::Type Result);

	/** Called when there is an error trying to travel to a local session */
	//void TravelLocalSessionFailure(UWorld *World, ETravelFailure::Type FailureType, const FString& ErrorString);

	/** Called after all the local players are registered in a session we're joining */
	void FinishJoinSession(EOnJoinSessionCompleteResult::Type Result);

	/** Callback which is intended to be called upon finding sessions */
	void OnSearchSessionsComplete(bool bWasSuccessful);


};
