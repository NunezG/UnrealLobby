// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealLobbyGameMode.h"
#include "Online/MyOnlineBeaconHost.h"
#include "Online/MyPartyBeaconHost.h"

#include "LobbyBeaconGameMode.generated.h"

/**
 * 
 */
UCLASS()
class UNREALLOBBY_API ALobbyBeaconGameMode : public AUnrealLobbyGameMode
{
	GENERATED_BODY()
	
	
	
private:
	virtual void BeginPlay() override;

	/** URL to travel to after pending network operations */
	FString TravelURL;

	/** Transient properties of a session during game creation/matchmaking */
	//	FGameSessionParams CurrentSessionParams;
	/** Current host settings */
	TSharedPtr<class FOnlineSessionSettings> HostSettings;
public:
	void HostLobby();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		AMyOnlineBeaconHost* BeaconHost;



	void StartPartyHost();
	void AddHostToBeaconHost(AOnlineBeaconHostObject* Host);

	UFUNCTION(BlueprintCallable)
		void CreateBeaconHost();


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		AMyPartyBeaconHost* PartyHost;

	bool HostGame(FName InSessionName, const FString& InTravelURL);

	bool HostSession(FName InSessionName, const FString& GameType, const FString& MapName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers);

	int PartySessionsConter = 0;
	int LobbySessionsConter = 0;

	TArray<AMyLobbyBeaconHostObject*> LobbyHosts;
	TArray<AMyPartyBeaconHost*> PartyHosts;


};
